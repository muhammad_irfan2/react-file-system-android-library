package com.btpn.lib.filesystem.service.file;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.btpn.lib.filesystem.handler.file.FileHandler;
import com.btpn.lib.filesystem.model.Response;
import com.btpn.lib.filesystem.service.base.BaseServiceImpl;
import com.btpn.lib.filesystem.handler.file.FileHandlerImpl;
import com.facebook.react.ReactActivity;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.modules.core.PermissionListener;

import java.io.File;

/**
 * Created by anka on 12/21/17.
 */

public class FileSystemServiceImpl extends BaseServiceImpl implements FileSystemService, PermissionListener {

    private String mFilename;
    private String mContent;
    private FileHandler mFileHandler;

    private final String TAG = FileSystemServiceImpl.class.getName();
    private final int REQUEST_CODE_ASK_PERMISSIONS = 101;

    public FileSystemServiceImpl(ReactContext context) {
        super(context);

        mFileHandler = new FileHandlerImpl(getContext());
    }

    private void requestPermission() {

        if (!ActivityCompat.shouldShowRequestPermissionRationale(getContext().getCurrentActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ((ReactActivity) getContext().getCurrentActivity()).requestPermissions(
                    new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CODE_ASK_PERMISSIONS,
                    this);
        }

        ((ReactActivity) getContext().getCurrentActivity()).requestPermissions(
                new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_CODE_ASK_PERMISSIONS,
                this);
    }

    private void openFile() {
        File file = mFileHandler.create(mFilename);

        if (file == null) {
            super.sendPromise(new Response(Response.SDK_RESPONSE_INTERNAL_ERROR, null, "Unable to create temp file.", false));
            return;
        }

        if (!mFileHandler.write(file, mContent)) {
            super.sendPromise(new Response(Response.SDK_RESPONSE_INTERNAL_ERROR, null, "Unable to write to temp file.", false));
            return;
        }

        if (mFileHandler.open(file)) {
            super.sendPromise(new Response(Response.SDK_RESPONSE_OK, file.getAbsolutePath(), "File opened successfully.", true));
        } else {
            super.sendPromise(new Response(Response.SDK_RESPONSE_INTERNAL_ERROR, null, "Unable to open file.", false));
        }
    }

    @Override
    public void generateFile(@NonNull String filename, @NonNull String content, Promise promise) {
        super.setPromise(promise);

        this.mFilename = filename;
        this.mContent = content;

        int hasPermission = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (hasPermission != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        } else {
            openFile();
        }
    }

    @Override
    public boolean onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean granted = false;

        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                granted = true;
                openFile();
            }
        }

        if (!granted) {
            super.sendPromise(new Response(Response.SDK_RESPONSE_INTERNAL_ERROR, null, "Access storage denied", false));
        }

        return granted;
    }
}
