package com.btpn.lib.filesystem.service.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.btpn.lib.filesystem.model.Response;

/**
 * Created by anka on 9/22/17.
 */

public interface BaseService {
    void sendEvent(@NonNull String event, @Nullable Response response);
    void sendPromise(@Nullable Response response);
}
