package com.btpn.lib.filesystem.service.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.btpn.lib.filesystem.R;
import com.btpn.lib.filesystem.model.Response;
import com.btpn.lib.filesystem.util.LogUtils;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.gson.Gson;

/**
 * Created by anka on 9/22/17.
 */

public class BaseServiceImpl implements BaseService{

    private ReactContext mContext;
    private Promise mPromise;

    private static final String TAG = BaseServiceImpl.class.getSimpleName();

    public BaseServiceImpl(ReactContext context) {
        this.mContext = context;
    }

    public ReactContext getContext() {
        return mContext;
    }

    public void setContext(ReactContext context) {
        this.mContext = context;
    }

    public Promise getPromise() {
        return mPromise;
    }

    public void setPromise(Promise promise) {
        this.mPromise = promise;
    }

    @Override
    public void sendEvent(@NonNull String event, @Nullable Response response) {
        try {
            mContext
                    .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                    .emit(event, new Gson().toJson(response));
        } catch (NullPointerException e) {
            LogUtils.log(Log.WARN, TAG, e.getMessage());
        }
    }

    @Override
    public void sendPromise(@Nullable Response response) {
        try {
            mPromise.resolve(response.toWritableMap());
        } catch (NullPointerException e) {
            LogUtils.log(Log.WARN, TAG, e.getMessage());
        }
    }
}