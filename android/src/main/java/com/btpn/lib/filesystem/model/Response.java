package com.btpn.lib.filesystem.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;

/**
 * Created by anka on 9/23/17.
 */

public class Response implements Parcelable {
    private int code;
    private String filename;
    private String message;
    private boolean success;

    public static final String RESPONSE_CODE = "code";
    public static final String RESPONSE_FILENAME = "filename";
    public static final String RESPONSE_MESSAGE = "message";
    public static final String RESPONSE_SUCCESS = "success";

    public static final int SDK_RESPONSE_INTERNAL_ERROR = 500;
    public static final int SDK_RESPONSE_OK = 200;

    public Response() {
    }

    public Response(int code, String filename, String message, boolean success) {
        this.code = code;
        this.filename = filename;
        this.message = message;
        this.success = success;
    }


    protected Response(Parcel in) {
        code = in.readInt();
        filename = in.readString();
        message = in.readString();
        success = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
        dest.writeString(filename);
        dest.writeString(message);
        dest.writeByte((byte) (success ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Response> CREATOR = new Creator<Response>() {
        @Override
        public Response createFromParcel(Parcel in) {
            return new Response(in);
        }

        @Override
        public Response[] newArray(int size) {
            return new Response[size];
        }
    };

    public WritableMap toWritableMap() {
        WritableMap map = Arguments.createMap();

        map.putInt(Response.RESPONSE_CODE, getCode());
        map.putString(Response.RESPONSE_FILENAME, getFilename());
        map.putString(Response.RESPONSE_MESSAGE, getMessage());
        map.putBoolean(Response.RESPONSE_SUCCESS, isSuccess());

        return map;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
