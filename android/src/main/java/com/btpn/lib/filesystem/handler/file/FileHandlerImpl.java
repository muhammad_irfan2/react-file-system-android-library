package com.btpn.lib.filesystem.handler.file;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.btpn.lib.filesystem.util.LogUtils;
import com.facebook.react.bridge.ReactContext;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by anka on 12/21/17.
 */

public class FileHandlerImpl implements FileHandler {

    private ReactContext mContext;

    static Map<String, String> types = new HashMap<>();

    public FileHandlerImpl(ReactContext context) {
        this.mContext = context;
    }

    static {
        types.put(".doc", "application/msword");
        types.put(".docx", "application/msword");
        types.put(".pdf", "application/pdf");
        types.put(".ppt", "application/vnd.ms-powerpoint");
        types.put(".pptx", "application/vnd.ms-powerpoint");
        types.put(".xls", "application/vnd.ms-excel");
        types.put(".xlsx", "application/vnd.ms-excel");
        types.put(".txt", "text/plain");
        types.put(".jpg", "image/jpeg");
        types.put(".jpeg", "image/jpeg");
        types.put(".png", "image/jpeg");
    }

    public static final String TAG = FileHandlerImpl.class.getName();


    public File create(String filename) {
        File root = this.mContext.getExternalFilesDir(null);
        File file = new File(root + "/" + filename);

        return file;
    }

    public boolean write(File file, String content) {
        try {
            FileOutputStream os = new FileOutputStream(file, false);
            os.write(Base64.decode(content, 0));
            os.flush();
            os.close();

            return true;
        } catch (FileNotFoundException e) {
            LogUtils.log(Log.ERROR, TAG, e.getMessage());
        } catch (IOException e) {
            LogUtils.log(Log.ERROR, TAG, e.getMessage());
        }

        return false;
    }

    public boolean open(File file) {
        if (!file.exists()) {
            return  false;
        }

        Uri uri;

        if (Build.VERSION.SDK_INT < 21) {
            uri = Uri.fromFile(file);
        } else {
            String packageName = this.mContext.getApplicationContext().getPackageName();
            String authority =  new StringBuilder(packageName).append(".provider").toString();

            uri = FileProvider.getUriForFile(this.mContext, authority, file);
        }
        
        String ext = MimeTypeMap.getFileExtensionFromUrl(uri.toString());

        Intent intent = new Intent(Intent.ACTION_VIEW);

        intent.setDataAndType(uri, types.get(ext));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        this.mContext.getCurrentActivity().startActivity(intent);

        return true;
    }
}
