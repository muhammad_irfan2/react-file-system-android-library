package com.btpn.lib.filesystem.handler.file;

import com.facebook.react.bridge.ReactContext;

import java.io.File;

/**
 * Created by anka on 12/24/17.
 */

public interface FileHandler {
    File create(String filename);
    boolean write(File file, String content);
    boolean open(File file);
}
