package com.btpn.lib.filesystem.module.base;

import com.facebook.react.bridge.ReactApplicationContext;

/**
 * Created by anka on 9/23/17.
 */

public interface BaseModule {

    ReactApplicationContext getContext();
    void setName(String name);
    String getName();
}
