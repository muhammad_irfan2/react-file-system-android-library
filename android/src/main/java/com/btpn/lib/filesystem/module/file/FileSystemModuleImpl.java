package com.btpn.lib.filesystem.module.file;

import android.support.annotation.NonNull;

import com.btpn.lib.filesystem.module.base.BaseModuleImpl;
import com.btpn.lib.filesystem.service.file.FileSystemServiceImpl;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactMethod;

/**
 * Created by anka on 12/21/17.
 */

public class FileSystemModuleImpl extends BaseModuleImpl implements FileSystemModule {
    private FileSystemServiceImpl mFileSystemService;

    public FileSystemModuleImpl(ReactApplicationContext reactContext, String name) {
        super(reactContext, name);

        mFileSystemService = new FileSystemServiceImpl(reactContext);
    }


    @ReactMethod
    @Override
    public void generateFile(@NonNull String filename, @NonNull String content, Promise promise) {
        mFileSystemService.generateFile(filename, content, promise);
    }
}
