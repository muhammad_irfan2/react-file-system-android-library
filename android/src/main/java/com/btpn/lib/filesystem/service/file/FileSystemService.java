package com.btpn.lib.filesystem.service.file;

import android.support.annotation.NonNull;

import com.facebook.react.bridge.Promise;

/**
 * Created by anka on 12/21/17.
 */

public interface FileSystemService {
    void generateFile(@NonNull String filename, @NonNull String content, Promise promise);
}
