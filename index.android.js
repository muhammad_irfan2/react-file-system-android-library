'use strict'

import { NativeModules } from 'react-native';

const { 
    FileSystem
} = NativeModules;

export {    
    FileSystem
}