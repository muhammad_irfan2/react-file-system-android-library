# React Google Vision Library

This project serves as a android google vision to create custom react native native modules that can later be installed through NPM and easily be used in production.

## Support

* React Native >= 0.48.3 
* Google Vision 
* Android SDK 25


## How to Use

### 1. Install

Install the library via npm.

```
npm install --save git+https://git.ecommchannels.com/Anka.Wirawan/react-google-vision-android-library.git
```

### 2. Link the library:

Add the following to android/settings.gradle:

```
include ':react-google-vision-android-library'
project(':react-google-vision-android-library').projectDir = new File(settingsDir, '../node_modules/react-google-vision-android-library/android')
```

Add the following to android/app/build.gradle:

```
dependencies {
    compile project(':react-google-vision-android-library')
}
```

Add the following to android/app/src/main/java/**/MainApplication.java:

```
package com.your.package;

import com.btpn.lib.cashlezwrapper.CashlezPackage;  // add this for react-google-vision-android-library

public class MainApplication extends Application implements ReactApplication {

    @Override
    protected List<ReactPackage> getPackages() {
        return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            new GoogleVisionPackage()     // add this for react-google-vision-android-library
        );
    }
}
```
### 3. Import module

Simply import/require it by the name defined in your library's package.json:

```
import { GoogleVisionScanner } from 'react-google-vision-android-library';
```

### 4. Usage

Below is list of some function and how to integrate with your project.

#### Barcode Module

```
async openBarcodeScanner() {
try {
    const barcode = await GoogleVisionScanner.openBarcodeScanner();

    this.setState({
    itemProductCode: barcode,
    });
} catch (error) {
    console.warn(error);
}
}
```



>K E E P C A L M A N D S H A R E K N O W L E D G E
